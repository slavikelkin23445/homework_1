package com.company.Menu.Items;

import com.company.Menu.MenuItem;

public class ExitMenuItem implements MenuItem {
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void execute() {
        System.out.println("Thanks for using this program");
    }

    @Override
    public boolean ifFinal() {
        return true;
    }
}
