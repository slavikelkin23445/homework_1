package com.company.Menu.Items;

import com.company.Menu.MenuItem;
import com.company.Menu.PointAndCircle.Circle;

public class CreateCircleMenuItem implements MenuItem {
    private Circle circle;

    public CreateCircleMenuItem(Circle circle) {
        this.circle = circle;
    }

    @Override
    public String getName() {
        return "Create circle";
    }

    @Override
    public void execute() {
        circle.createCircle();
    }
}
