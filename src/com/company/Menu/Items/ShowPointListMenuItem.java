package com.company.Menu.Items;

import com.company.Menu.MenuItem;
import com.company.Menu.PointAndCircle.PointList;
import com.company.Menu.PointAndCircle.UI;

public class ShowPointListMenuItem implements MenuItem {
    private PointList pl;
    private UI ui;

    public ShowPointListMenuItem(PointList pl, UI ui) {
        this.pl = pl;
        this.ui = ui;
    }

    @Override
    public String getName() {
        return "Show point-list";
    }

    @Override
    public void execute() {
        ui.showPointList(pl);
    }
}
