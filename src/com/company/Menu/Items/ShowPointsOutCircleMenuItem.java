package com.company.Menu.Items;

import com.company.Menu.MenuItem;
import com.company.Menu.PointAndCircle.Circle;
import com.company.Menu.PointAndCircle.PointList;

public class ShowPointsOutCircleMenuItem implements MenuItem {

    private Circle circle;
    private PointList pl;

    public ShowPointsOutCircleMenuItem(Circle circle, PointList pl) {
        this.circle = circle;
        this.pl = pl;
    }

    @Override
    public String getName() {
        return "Show points outside circle";
    }

    @Override
    public void execute() {
        circle.pointOutCircle(pl);
    }
}
