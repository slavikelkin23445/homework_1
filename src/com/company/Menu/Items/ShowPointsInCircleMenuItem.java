package com.company.Menu.Items;

import com.company.Menu.MenuItem;
import com.company.Menu.PointAndCircle.Circle;
import com.company.Menu.PointAndCircle.PointList;

public class ShowPointsInCircleMenuItem implements MenuItem {

    private Circle circle;
    private PointList pl;

    public ShowPointsInCircleMenuItem(Circle circle, PointList pl) {
        this.circle = circle;
        this.pl = pl;
    }

    @Override
    public String getName() {
        return "Show points inside circle";
    }

    @Override
    public void execute() {
        circle.containsPoint(pl);
    }
}
