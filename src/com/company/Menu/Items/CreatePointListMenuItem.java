package com.company.Menu.Items;

import com.company.Menu.MenuItem;
import com.company.Menu.PointAndCircle.PointList;

public class CreatePointListMenuItem implements MenuItem {

    private PointList pl;

    public CreatePointListMenuItem(PointList pl) {
        this.pl = pl;
    }

    @Override
    public String getName() {
        return "Create point-list";
    }

    @Override
    public void execute() {
        pl.initPointList();
        pl.fillMassWithPoints();
    }
}
