package com.company.Menu.Items;

import com.company.Menu.MenuItem;
import com.company.Menu.PointAndCircle.Circle;
import com.company.Menu.PointAndCircle.Point;

import java.util.Scanner;

public class ChangeCircleParamsMenuItem implements MenuItem {
    private Circle circle;
    private Scanner scanner;

    public ChangeCircleParamsMenuItem(Circle circle) {
        this.circle = circle;
    }

    @Override
    public String getName() {
        return "Change circle's characteristics";
    }

    @Override
    public void execute() {
        circle.createCircle();
    }
}
