package com.company.Menu.Items;

import com.company.Menu.MenuItem;
import com.company.Menu.PointAndCircle.PointList;

public class AddPointMenuItem implements MenuItem {

    private PointList pl;

    public AddPointMenuItem(PointList pl) {
        this.pl = pl;
    }

    @Override
    public String getName() {
        return "Add point in list";
    }

    @Override
    public void execute() {
        pl.addPointToList();
    }
}
