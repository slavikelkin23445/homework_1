package com.company.Menu;

public interface MenuItem {
    String getName();
    void execute();
    default boolean ifFinal(){
        return false;
    }
}
