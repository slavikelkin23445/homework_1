package com.company.Menu.PointAndCircle;

import java.util.Scanner;

public class UI {
    private Scanner scanner;

    public UI(Scanner scanner) {
        this.scanner = scanner;
    }

    public Point getUserPoint(Point p) {
        System.out.println("Enter coordinates of your point (x, y): ");

        System.out.print("Your X: ");
        p.x = scanner.nextDouble();
        scanner.nextLine();

        System.out.print("Your Y: ");
        p.y = scanner.nextDouble();
        scanner.nextLine();

        System.out.println("-----------------------------------------------");
        return p;
    }

    public void getUserCircleCenter(Point center){
        System.out.println("Coordinates of center of circle: ");
        getUserPoint(center);
    }

    public double getUserCircleRadius(double radius){
        System.out.println("Enter length of circle's radius: ");
        radius = scanner.nextDouble();
        scanner.nextLine();
        return radius;
    }

    public void showPointsInCircle(Point p){
        System.out.println("X: " + p.getX());
        System.out.println("Y: " + p.getY());
        System.out.println("------------------------------------------------");
    }

    public void showPointsOutCircle(Point p){
        System.out.println("X: " + p.getX());
        System.out.println("Y: " + p.getY());
        System.out.println("------------------------------------------------");
    }

    public boolean userChoice(){
        int userChoice;
        System.out.println("Do you want to add one more point (1 - Yes, 2 - No): ");
        userChoice = scanner.nextInt();
        scanner.nextLine();
        return userChoice == 1;
    }

    public void showPointList(PointList pl){
        for (int i = 0; i < pl.getListSize(); i++) {
            System.out.println("Point " + (i+1) + ":");
            System.out.println("Your X: " + pl.getPointFromList(i).getX());
            System.out.println("Your Y: " + pl.getPointFromList(i).getY());
        }
    }
}
