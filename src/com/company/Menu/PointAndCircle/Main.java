package com.company.Menu.PointAndCircle;

import com.company.Menu.Items.*;
import com.company.Menu.Menu;
import com.company.Menu.MenuItem;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        UI ui = new UI(scanner);

        Circle circle = new Circle(new Point(scanner),scanner, ui);
        PointList list = new PointList(scanner, ui);

        Menu menu = new Menu(scanner, new MenuItem[]{
                new CreatePointListMenuItem(list),
                new CreateCircleMenuItem(circle),
                new AddPointMenuItem(list),
                new ChangeCircleParamsMenuItem(circle),
                new ShowPointListMenuItem(list, ui),
                new ShowPointsInCircleMenuItem(circle, list),
                new ShowPointsOutCircleMenuItem(circle, list),
                new ExitMenuItem()
        });
        menu.run();


    }
}