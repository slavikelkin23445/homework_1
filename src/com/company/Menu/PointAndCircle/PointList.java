package com.company.Menu.PointAndCircle;

import java.util.Scanner;

public class PointList {
    private Point pointList[];
    private int listSize;
    private Scanner scanner;
    private UI ui;

    public PointList(Scanner scanner, UI ui) {
        this.scanner = scanner;
        this.ui = ui;
    }

    public int getListSize() {
        return listSize;
    }

    public Point getPointFromList(int counter){
        return pointList[counter];
    }

    public void initPointList(){
        System.out.println("Enter how many points you are going input: ");
        listSize = scanner.nextInt();
        scanner.nextLine();
        pointList = new Point[listSize];
    }

    public void fillMassWithPoints(){
        for(int i = 0; i < pointList.length; i++){
             pointList[i] = new Point(scanner);
             pointList[i] = ui.getUserPoint(new Point(scanner));
             if(i != pointList.length - 1 && ui.userChoice()){
                 continue;
             }else{
                 break;
             }
         }
    }

    public void addPointToList(){
        pointList = copyOf(pointList, listSize);
        pointList[listSize] = ui.getUserPoint(new Point(scanner));
        listSize++;
    }

    private Point[] copyOf(Point pl[], int size){
        pl = new Point[size + 1];
        for(int i = 0; i < pointList.length; i++){
            pl[i] = pointList[i];
        }
        return pl;
    }
}
