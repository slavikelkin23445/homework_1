package com.company.Menu.PointAndCircle;

import java.util.Scanner;

public class Circle {
    public Point center;
    public double radius;
    private Scanner scanner;
    private UI ui;

    public Circle(Point center, Scanner scanner, UI ui) {
        this.center = center;
        this.scanner = scanner;
        this.ui = ui;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public void createCircle(){
        ui.getUserCircleCenter(center);
        radius = ui.getUserCircleRadius(radius);
    }

    public void containsPoint(Point p){
        double distance = center.distanceTo(p);
        if(distance <= radius){
            ui.showPointsInCircle(p);
        }else{
            System.out.println("Point out this circle");
        }
    }

    public void containsPoint(PointList pl){
        System.out.println("Point in circle with those coordinates: ");
        for(int i = 0; i < pl.getListSize(); i++){
            if(pl.getPointFromList(i) != null) {
                double distance = center.distanceTo(pl.getPointFromList(i));
                if (distance <= radius) {
                    ui.showPointsInCircle(pl.getPointFromList(i));
                } else {
                    continue;
                }
            }
        }
    }

    public void pointOutCircle(PointList pl){
        System.out.println("Point out circle with those coordinates: ");
        for(int i = 0; i < pl.getListSize(); i++){
            if(pl.getPointFromList(i) != null) {
                double distance = center.distanceTo(pl.getPointFromList(i));
                if (distance >= radius) {
                    ui.showPointsOutCircle(pl.getPointFromList(i));
                } else {
                    continue;
                }
            }
        }
    }
}
