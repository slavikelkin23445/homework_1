package com.company.Menu.PointAndCircle;

import java.util.Scanner;

public class Point {
    public double x;
    public double y;
    private Scanner scanner;

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Point(Scanner scanner) {
        this.scanner = scanner;
    }

    public double distanceTo(Point p){
        double dx = p.x - this.x;
        double dy = p.y - this.y;
        double distanceSquare = Math.sqrt(dx*dx + dy*dy);
        return distanceSquare;
    }
}
